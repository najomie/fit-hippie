<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php
	global $naj_functions;
	$page_for_posts = get_option( 'woocommerce_shop_page_id' );
	$header_banner = get_field('bg_header', $page_for_posts);
	$title = get_field('intro_title', $page_for_posts);
?>

<?php if( $header_banner ): ?>
    <section class="page-banner" style="background-image:url(<?php echo $header_banner['url']; ?>)">
        <div class="text-banner">
            <div class="inner">
                <?php if( $title ): ?>
                    <h1><?php echo $title; ?> <?php if( $term_name ): ?>/ <?php echo $term_name; ?><?php endif; ?></h1>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<div class="container woocommerce">

	<section class="content">

		<div class="row post-flex">

				<div class="post-list">

				<?php if ( have_posts() ) : ?>

						<?php woocommerce_product_subcategories(); ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<article class="post product-post">
								<?php wc_get_template_part( 'content', 'product' ); ?>
							</article>

						<?php endwhile; // end of the loop. ?>

				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

					<?php wc_get_template( 'loop/no-products-found.php' ); ?>

				<?php endif; ?>


			</div>

			<aside class="blog-sidebar">
				<form class="search" method="get" action="<?php echo home_url();?>">
					<input type="hidden" name="post_type" value="product" />
					<input class="input-seach" type="text" name="s" placeholder="<?php _e('Recherche ...'); ?>"/>
					<button type="submit"><i class="fa fa-search"></i></button>
				</form>
				<?php dynamic_sidebar('woocommerce'); ?>
			</aside>

		</div>

		<?php
			/**
			 * woocommerce_after_shop_loop hook.
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action( 'woocommerce_after_shop_loop' );
		?>

	</section>

</div>

<?php get_footer( 'shop' ); ?>
