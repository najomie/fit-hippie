<?php get_header(); ?>

<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<?php
		$mainh_img   = get_field('main_headerbox_img');
		$mainh_link  = get_field('main_headerbox_link');
		$headerboxes = get_field('headerboxes');
		$instore = get_field('in_store');
	?>

	<section class="content">

		<div class="contain">
			<div class="main-headerbox">
				<div class="main-box">
					<a class="main" href="<?php echo $mainh_link; ?>">
						<img src="<?php echo $mainh_img['url']; ?>" alt="<?php echo $mainh_img['alt']; ?>" />
					</a>
				</div>
				<div class="inner">
					<a href="http://www.fithippie.co/infolettre/" class="link-button turquoise" target="_blank"><?php _e("inscrivez-vous à <br>l'infolettre"); ?></a>
					<a href="https://www.facebook.com/FitHippieCo/" class="link-button turquoise" target="_blank"><?php _e('Suivez-nous sur <br>Facebook'); ?></a>
				</div>
			</div>
		</div>

		<?php if( $headerboxes ): ?>
			<div class="headerboxes">
				<?php foreach( $headerboxes as $box ): ?>
					<?php
						$img   = $box['img'];
						$link  = $box['link'];
					?>
					<div class="block">
						<a href="<?php echo $link; ?>">
							<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" />
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<h2 class="section-title"><span><?php _e('Blogue'); ?></span></h2>
		<?php get_template_part('parts/home/latest-posts'); ?>

		<?php if( $instore ): ?>
			<h2 class="section-title"><span><?php _e('Dans la boutique'); ?></span></h2>
			<div class="instore">
				<div class="stores">
					<?php $counter = 0; foreach( $instore as $store ): $counter++; ?>
						<?php
							$img   = $store['img'];
							$link  = $store['link'];
							$text  = $store['link_text'];
						?>
						<div class="store space<?php echo $counter; ?>">
							<a href="<?php echo $link; ?>">
								<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" />
								<h6><?php echo $text; ?></h6>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
				<a href="<?php echo get_permalink(woocommerce_get_page_id('shop' )); ?>" class="button-link purple"><?php _e('Boutique Fit Hippie'); ?></a>
			</div>
		<?php endif; ?>


		<h2 class="section-title"><span><?php _e('Instagram'); ?></span></h2>
		<div class="insta-block home-block">
			<?php echo do_shortcode('[instagram-feed]'); ?>
		</div>

	</section>

<?php endwhile; endif; ?>

<?php get_footer();
