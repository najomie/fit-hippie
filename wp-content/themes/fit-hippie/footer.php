<?php global $naj_functions; ?>
		</div>

		<footer class="footer">

			<?php get_template_part('parts/socials'); ?>

			<div class="copyrights">
				<div class="container">
					<h2><strong><?php _e('©Fit hippie inc.'); ?> <?php echo date('Y'); ?></strong><br><?php _e('Tous droits réservés'); ?></h2>
				</div>
				<a class="logo" href="<?php echo site_url() ?>"><img src="<?php echo $naj_functions->imgURL('logo.png'); ?>" alt="<?php bloginfo('name') ?>" /></a>
			</div>

		</footer>

	</div>

	<?php wp_footer(); ?>
</body>
</html>
