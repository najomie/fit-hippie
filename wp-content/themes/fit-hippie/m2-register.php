<?php get_header(); ?>

<?php get_template_part('parts/page-header' ); ?>

	<section class="content">
		<div class="register member-layouts">
			<div class="member-content">
				<h1><?php _e('Inscription'); ?></h1>
				<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>

	</section>

<?php get_footer();
