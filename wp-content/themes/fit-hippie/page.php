<?php get_header(); ?>

<?php get_template_part('parts/page-header' ); ?>

<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<section class="content">

		<div class="page-layouts">
			<?php if( have_rows('blocs') ) : while( have_rows('blocs') ) : the_row();

				get_template_part('parts/page/layout', get_row_layout() );

			endwhile; else: ?>
				<?php the_content(); ?>
			<?php endif; ?>
		</div>

	</section>

<?php endwhile; endif; ?>

<?php get_footer();
