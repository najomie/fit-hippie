<?php get_header(); ?>

<?php
	global $naj_functions;
	$page_for_posts = get_option( 'page_for_posts' );
	$header_banner = get_field('bg_header', $page_for_posts);
	$header_text = get_field('add_text', $page_for_posts);
	$img = get_field('secondary_img', $page_for_posts);
	$title = get_field('intro_title', $page_for_posts);
	$buttons = get_field('buttons', $page_for_posts);

	$queried_object = get_queried_object();

	if( $queried_object ){
		$term_name = $queried_object->name;
	}

	$class = "";
?>

<?php if( $header_banner ): ?>
    <section class="page-banner" style="background-image:url(<?php echo $header_banner['url']; ?>)">
        <div class="text-banner">
            <div class="inner">
                <?php if( $title ): ?>
                    <h1><?php echo $title; ?> <?php if( $term_name ): ?>/ <?php echo $term_name; ?><?php endif; ?></h1>
                <?php endif; ?>
                <?php if( $img ): ?>
                    <img class="secondary-img" src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
                <?php endif; ?>
                <?php if( $header_text ): ?>
                    <p><?php echo $header_text; ?></p>
                <?php endif; ?>
                <?php if( $buttons ): ?>
                    <div class="buttons">
                        <?php foreach ( $buttons as $button ): ?>
                            <a class="button-link" href="<?php echo $button['link']; ?>"><?php echo $button['text']; ?></a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<section class="content container-fluid">

	<div class="row post-flex">

		<div class="post-list">
			<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

			<article class="post">

				<?php if ( has_post_thumbnail() ) : ?>
				<figure class="post-thumbnail" style="background-image:url(<?php the_post_thumbnail_url('single-post-image'); ?>);">
					<a class="overlay" href="<?php echo the_permalink(); ?>">
						<span><?php _e('En lire plus'); ?></span>
					</a>

					<span class="cats"><?php the_category( ', ' ); ?></span>
				</figure>
				<?php endif; ?>

				<div class="post-text">
					<h3><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<span class="dates"><?php echo get_the_time('j F Y'); ?></span>
					<p><?php echo $naj_functions->get_excerpt(230); ?></p>
				</div>
			</article>

			<?php endwhile; ?>

			<?php else: ?>

				<div class="no-results">
					<?php _e('Désolé. Aucun résultat pour votre recherche.'); ?>
				</div>

			<?php endif; ?>

			<div class="pagination"><?php $naj_functions->archive_pagination(); ?></div>

		</div>

		<aside class="blog-sidebar">
			<form class="search" method="get" action="<?php echo home_url();?>">
				<input class="input-seach" type="text" name="s" placeholder="<?php _e('Recherche ...'); ?>"/>
				<input type="hidden" name="post_type" value="post" />
				<button type="submit"><i class="fa fa-search"></i></button>
			</form>
			<?php dynamic_sidebar( 'main-sidebar' ); ?>
		</aside>

	</div>

</section>

<?php get_footer();
