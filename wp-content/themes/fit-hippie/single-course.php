<?php get_header(); ?>

<?php

	global $wp_query;
	$weeklies	= get_field('weeklies_groups');
	$crashes	= get_field('crash_courses');
	$mocks		= get_field('mock_exams');

	$thumb = $naj_functions->imgURL('single-fallback');

	if ( has_post_thumbnail() ){
		$thumb = get_the_post_thumbnail_url();
	}
?>

<div class="single-header" style="background-image:url(<?php echo $thumb; ?>);">
	<div class="caption">
		<div class="inner">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
</div>

<nav class="course-nav">
	<div class="inner">
		<?php if( $weeklies ): ?>
			<a href="<?php echo get_permalink(); ?>weeklies" class="<?php if( !array_key_exists( 'mock-exams', $wp_query->query_vars ) && !array_key_exists( 'crash-course', $wp_query->query_vars ) ): ?>active<?php endif;?>">
				<?php _e('Weeklies'); ?>
			</a>
		<?php endif; ?>
		<?php if( $crashes ): ?>
			<a href="<?php echo get_permalink(); ?>crash-course" class="<?php if( array_key_exists( 'crash-course', $wp_query->query_vars ) ): ?>active<?php endif;?>">
				<?php _e('Crash courses'); ?>
			</a>
		<?php endif; ?>
		<?php if( $mocks ): ?>
			<a href="<?php echo get_permalink(); ?>mock-exams" class="<?php if( array_key_exists( 'mock-exams', $wp_query->query_vars ) ): ?>active<?php endif;?>">
				<?php _e('Mock exams'); ?>
			</a>
		<?php endif; ?>
	</div>
</nav>

<section class="content" id="course-content">

	<?php if( !array_key_exists( 'mock-exams', $wp_query->query_vars ) && !array_key_exists( 'crash-course', $wp_query->query_vars ) ): ?>
		<section id="weeklies" class="section">
			<?php get_template_part('parts/course/weekly'); ?>
		</section>
	<?php endif; ?>

	<?php if( array_key_exists( 'crash-course', $wp_query->query_vars ) ): ?>
		<section id="crash-course" class="section">
			<?php get_template_part('parts/course/crash-course'); ?>
		</section>
	<?php endif; ?>

	<?php if( array_key_exists( 'mock-exams', $wp_query->query_vars ) ): ?>
		<section id="mock-exams" class="section">
			<?php get_template_part('parts/course/mock-exams'); ?>
		</section>
	<?php endif; ?>

</section>

<?php get_template_part('parts/page/layout-newsletter_block'); ?>

<?php get_footer();
