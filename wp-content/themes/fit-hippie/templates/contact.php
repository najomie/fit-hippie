<?php /* Template Name: Page: Contact */ get_header(); ?>

<?php echo do_shortcode('[google-map]'); ?>

<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<section class="content">

		<div class="page-layouts">
			<?php if( have_rows('page_content') ) : while( have_rows('page_content') ) : the_row();

				get_template_part('parts/page/layout', get_row_layout() );

			endwhile; endif; ?>
		</div>

	</section>

<?php endwhile; endif; ?>

<?php get_footer();
