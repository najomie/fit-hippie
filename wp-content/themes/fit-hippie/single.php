<?php get_header(); ?>

<?php
	global $naj_functions;
	$banner = get_field('post_banner', 'options');
	if( is_singular( 'podcast' ) ){
		$banner = get_field('podcast_banner', 'options');
	}
?>

<section class="content container-fluid">

	<div class="row post-flex">

		<div class="post-single">
			<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

				<div class="title-section">
					<h1><?php the_title(); ?></h1>

					<aside class="meta">
						<span class="loop-date"><?php the_time('j F Y') ?></span> /
						<span class="loop-cats"><?php the_category(', '); ?></span>
					</aside>
				</div>

				<article class="post">

					<?php the_content(); ?>

				</article>

			<?php endwhile; endif; ?>

			<div class="pagination"><?php $naj_functions->archive_pagination(); ?></div>

		</div>

		<aside class="blog-sidebar">
			<form class="search" method="get" action="<?php echo home_url();?>">
				<input class="input-seach" type="text" name="s" placeholder="<?php _e('Recherche ...'); ?>"/>
				<input type="hidden" name="post_type" value="post" />
				<button type="submit"><i class="fa fa-search"></i></button>
			</form>
			<?php dynamic_sidebar( 'blog-sidebar' ); ?>
		</aside>

	</div>

</section>

<?php get_footer();
