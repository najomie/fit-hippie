jQuery(document).ready(function($) {

	$('#wpadminbar').addClass('Fixed');
	$("#courses").appendTo(".sub-menu");

	// Mobile menu
	$('.hamburger').click(function(){
		$(this).toggleClass('is-active');
		$('.mobile-menu').slideToggle(300);
		return false;
	});

	$('.menu .menu-item-has-children').hover(function(e){
		$(this).toggleClass('menu-active');
	});

	$('#courses .menu-item').hover(function(e){
		$(this).toggleClass('sub-active');
	});


	var replaceWooCommerceQuantityButtons = function()
	{
		$(".quantity").each(function(i, el)
		{
			var $quantity = $(el),
				$button = $quantity.find('.qty');

			if($quantity.hasClass('buttons_added'))
				return;

			$quantity.addClass('buttons_added');

			$button.before('<input type="button" value="-" class="plusminus minus">');
			$button.after('<input type="button" value="+" class="plusminus plus">');
		});
	};

	replaceWooCommerceQuantityButtons();

	$("body").on('click', 'input[type="button"].plusminus', function()
	{
		var $this = $(this),
			$quantity = $this.prev(),
			add = 1;

		if($this.hasClass('minus'))
		{
			$quantity = $this.next();
			add = -1;
		}

		var newVal = parseInt($quantity.val(), 10) + add;

		if(newVal < 0)
			newVal = 0;

		$quantity.val(newVal).trigger( 'change' );
	});

	$(".quantity .input-text").attr('type', 'text');

	// Slick Slider
	if ( $.fn.slick ) {

		var options = {
		  dots: true,
		  arrows: false,
		  pauseOnHover: false,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  fade: false,
		  draggable: true,
		  swipe: true,
		  autoplay: true,
		  speed: 1000,
		  autoplaySpeed: 6000
	  };

		$('.testimonials-slider').slick(options);
	}

	if ( $.fn.matchHeight ) {
		$('.latest .post-details h3').matchHeight();
		$('.latest .post-details').matchHeight();
		$('.post-flex .post-list .post h3').matchHeight();
		$('.post-flex .post-list .post .post-text').matchHeight();
	}

	// Account - Convert label into placeholders

	$(".form-membership :input").each(function(index, elem) {
		var eId = $(elem).attr("id");
		var label = null;
		if (eId && (label = $(elem).siblings("label[for="+eId+"]")).length == 1) {
			 $(elem).attr("placeholder", $(label).html());
			 $(label).remove();
		}
	});

});
