<?php get_header(); ?>

<?php get_template_part('parts/page-header' ); ?>

	<section class="content">

		<div class="account member-layouts">
			<div class="account-menu">
				<?php wp_nav_menu( array( 'theme_location' => 'account-nav', 'container' => 'account-nav', 'container_class' => 'account-nav-wrap', 'fallback_cb' => 'false' )); ?>
			</div>
			<div class="member-content">
				<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
			<div class="member-sidebar">
				<img src="<?php echo $naj_functions->imgURL('account-banner.jpg'); ?>" alt="" />
			</div>
		</div>

	</section>

<?php get_footer();
