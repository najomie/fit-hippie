<?php $socials = get_field('socials_fr', 'options'); ?>
<?php if( $socials ): ?>
    <ul class="socials">
        <?php foreach ($socials as $social): ?>
            <li><a class="<?php echo $social['icon']; ?>" href="<?php echo $social['link']; ?>" target="_blank" onclick="gaTracker('send', 'event', 'category', 'action', {'nonInteraction': 1});"><i class="fa <?php echo $social['icon']; ?>"></i></a></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
