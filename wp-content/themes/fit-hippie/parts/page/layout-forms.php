<?php global $naj_functions;

$layout = get_row_layout() . '_';

$title  = get_sub_field(''.$layout.'section_title');
$anchor = get_sub_field(''.$layout.'anchor');

$forms    = get_sub_field(''.$layout.'formulas');
$banner   = get_sub_field(''.$layout.'bg');

?>
<div id="<?php echo $anchor; ?>" class="page-layout <?php echo get_row_layout(); ?>">
    <?php if( $title): ?>
        <h2 class="section-title"><span><?php echo $title; ?></span></h2>
    <?php endif; ?>
    <?php if( $forms ): ?>
        <div class="formulas">
            <div class="forms">
                <div class="banner" style="background-image:url(<?php echo $banner['url']; ?>);"></div>
                <?php foreach ( $forms as $form ): ?>
                    <div class="form">
                        <div class="price-wrap">
                            <?php if( $form['price'] ): ?>
                                <span class="price"><?php echo $form['price']; ?> <span class="devise">$</span></span>
                            <?php endif; ?>
                            <?php if( $form['duration'] ): ?>
                                <span class="duration"><?php echo $form['duration']; ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="text">
                            <?php echo $form['text']; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
