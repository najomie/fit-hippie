<?php global $naj_functions;

$layout = get_row_layout() . '_';

$title  = get_sub_field(''.$layout.'section_title');
$anchor = get_sub_field(''.$layout.'anchor');

$bg     = get_sub_field(''.$layout.'bg');
$i_title  = get_sub_field(''.$layout.'title');
$youtube  = get_sub_field(''.$layout.'youtube_link');


?>
<div id="<?php echo $anchor; ?>" class="page-layout <?php echo get_row_layout(); ?>">
    <?php if( $title): ?>
        <h2 class="section-title"><span><?php echo $title; ?></span></h2>
    <?php endif; ?>
    <?php if( $i_title ): ?>
        <h4><?php echo $i_title; ?></h4>
    <?php endif; ?>
    <div class="video-wrapper" style="background-image: url(<?php echo $bg['url']; ?>);">
        <div class="video-wrap">
            <div class="inner">
                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/oR4jov7IAFM?rel=0&amp;showinfo=0&color=white" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
