<?php global $naj_functions;

$layout = get_row_layout() . '_';

$title  = get_sub_field(''.$layout.'section_title');
$anchor = get_sub_field(''.$layout.'anchor');

$teams    = get_sub_field('teams');

?>
<div id="<?php echo $anchor; ?>" class="page-layout <?php echo get_row_layout(); ?>">
    <?php if( $title): ?>
        <h2 class="section-title"><span><?php echo $title; ?></span></h2>
    <?php endif; ?>
    <?php if( $teams ): ?>
        <div class="team">
            <?php foreach ( $teams as $member ): ?>
                <div class="member">
                    <figure class="photo" style="background-image:url(<?php echo $member['photo']['url']; ?>);"></figure>
                    <div class="bio-wrap">
                        <h3><?php echo $member['name']; ?></h3>
                        <h5><?php echo $member['title']; ?></h5>
                        <p><?php echo $member['bio'] ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
