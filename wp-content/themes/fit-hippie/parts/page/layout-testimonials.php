<?php global $naj_functions;

$layout = get_row_layout() . '_';

$title  = get_sub_field(''.$layout.'section_title');
$anchor = get_sub_field(''.$layout.'anchor');

$testimonials    = get_sub_field(''.$layout.'testi');

?>
<div id="<?php echo $anchor; ?>" class="page-layout <?php echo get_row_layout(); ?>">
    <?php if( $title): ?>
        <h2 class="section-title"><span><?php echo $title; ?></span></h2>
    <?php endif; ?>
    <?php if( $testimonials ): ?>
        <div class="testimonials-slider">
            <?php foreach ( $testimonials as $testimonial ): ?>
                <div class="testimonial">
                    <figure class="testi-img" style="background-image:url(<?php echo $testimonial['img']['url']; ?>);"></figure>
                    <h3><?php echo $testimonial['name']; ?>, <span><?php echo $testimonial['city']; ?></span></h3>
                    <p><?php echo $testimonial['testi'] ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
