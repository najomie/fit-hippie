<?php global $naj_functions;

$layout = get_row_layout() . '_';

$title  = get_sub_field(''.$layout.'section_title');
$anchor = get_sub_field(''.$layout.'anchor');

$blocs    = get_sub_field(''.$layout.'blocs');
$button   = get_sub_field(''.$layout.'button');
$link  = get_sub_field(''.$layout.'button_link');

?>
<div id="<?php echo $anchor; ?>" class="page-layout <?php echo get_row_layout(); ?>">
    <?php if( $title): ?>
        <h2 class="section-title"><span><?php echo $title; ?></span></h2>
    <?php endif; ?>
    <?php if( $blocs ): ?>
        <div class="blocs">
            <?php foreach ( $blocs as $bloc ): ?>
                <div class="bloc">
                    <figure  class="pictos" style="background-image:url(<?php echo $bloc['img']['url']; ?>);"></figure>
                    <p><?php echo $bloc['text']; ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php if( $button ): ?>
        <a class="button-link" href="<?php echo $link; ?>"><?php echo $button; ?></a>
    <?php endif; ?>
</div>
