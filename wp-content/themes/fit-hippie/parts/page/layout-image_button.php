<?php global $naj_functions;

$layout = get_row_layout() . '_';

$title  = get_sub_field(''.$layout.'section_title');
$anchor = get_sub_field(''.$layout.'anchor');

$blocs  = get_sub_field(''.$layout.'blocs');
$count  = count( $blocs );

?>
<div id="<?php echo $anchor; ?>" class="page-layout <?php echo get_row_layout(); ?>">
    <?php if( $title): ?>
        <h2 class="section-title"><span><?php echo $title; ?></span></h2>
    <?php endif; ?>
    <?php if( $blocs ): ?>
        <div class="blocs <?php echo $count; ?>-block">
            <?php foreach ( $blocs as $bloc ): ?>
                <div class="bloc">
                    <?php if( $bloc['title'] ): ?>
                        <h4><?php echo $bloc['title']; ?></h4>
                    <?php endif; ?>
                    <figure class="bloc-img" style="background-image:url(<?php echo $bloc['img']['url']; ?>);"></figure>
                    <?php if( $bloc['button'] ): ?>
                        <a href="<?php echo $bloc['link']; ?>" class="button-link"><?php echo $bloc['button']; ?></a>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
