<?php global $naj_functions;

$layout = get_row_layout() . '_';

$title  = get_sub_field(''.$layout.'section_title');
$anchor = get_sub_field(''.$layout.'anchor');

$center = get_sub_field(''.$layout.'centered');
$left   = get_sub_field(''.$layout.'left');
$right  = get_sub_field(''.$layout.'right');


?>
<div id="<?php echo $anchor; ?>" class="page-layout <?php echo get_row_layout(); ?>">
    <?php if( $title): ?>
        <h2 class="section-title"><span><?php echo $title; ?></span></h2>
    <?php endif; ?>
    <div class="centered">
        <?php echo $center; ?>
    </div>
    <div class="intro-bloc-inner">
        <div class="left">
            <?php echo $left; ?>
        </div>
        <div class="right">
            <?php echo $right; ?>
        </div>
    </div>
</div>
