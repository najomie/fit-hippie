<?php global $naj_functions;

$layout = get_row_layout() . '_';

$title  = get_sub_field(''.$layout.'section_title');
$anchor = get_sub_field(''.$layout.'anchor');
$img    = get_sub_field(''.$layout.'img');
$text   = get_sub_field(''.$layout.'text');
$color  = get_sub_field(''.$layout.'color');
$pos    = get_sub_field(''.$layout.'pos');


?>
<div id="<?php echo $anchor; ?>" class="page-layout <?php echo get_row_layout(); ?>">
    <?php if( $title): ?>
        <h2 class="section-title"><span><?php echo $title; ?></span></h2>
    <?php endif; ?>
    <div class="inner-layout <?php echo $pos; ?>" style="background-image:url(<?php echo $img['url']; ?>);">
        <div class="container">
            <div class="text" style="color: <?php echo $color; ?>">
                <?php echo $text; ?>
            </div>
        </div>
    </div>
</div>
