<?php
global $naj_functions;
$args = array( 'posts_per_page' => 5 );
$latest = get_posts( $args ); ?>
<div class="latest-posts">
    <div class="latests">
        <?php foreach ( $latest as $post ) : setup_postdata( $post ); ?>
        	<div class="latest">
                <a href="<?php the_permalink(); ?>"><figure class="post-thumbnail" style="background-image:url(<?php the_post_thumbnail_url('thumbnail'); ?>);"></figure></a>
                <div class="post-details">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <p><?php echo $naj_functions->get_excerpt(80); ?></p>
                </div>
        	</div>
        <?php endforeach; wp_reset_postdata(); ?>
    </div>
    <a href="<?php echo get_post_type_archive_link( 'post' ); ?>" class="button-link purple"><?php _e('Blogue Fit Hippie'); ?></a>
</div>
