<?php

    $header_banner = get_field('bg_header');
    $header_text = get_field('add_text');
    $img = get_field('secondary_img');
    $title = get_field('intro_title');
    $buttons = get_field('buttons');

    $class = "";
?>

<?php if( $header_banner ): ?>
    <section class="page-banner" style="background-image:url(<?php echo $header_banner['url']; ?>)">
        <div class="text-banner">
            <div class="inner">
                <?php if( $title ): ?>
                    <h1><?php echo $title; ?></h1>
                <?php endif; ?>
                <?php if( $img ): ?>
                    <img class="secondary-img" src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
                <?php endif; ?>
                <?php if( $header_text ): ?>
                    <p><?php echo $header_text; ?></p>
                <?php endif; ?>
                <?php if( $buttons ): ?>
                    <div class="buttons">
                        <?php foreach ( $buttons as $button ): ?>
                            <a class="button-link" href="<?php echo $button['link']; ?>"><?php echo $button['text']; ?></a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
