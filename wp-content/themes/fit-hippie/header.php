<?php global $naj_functions;
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html id="top" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="dns-prefetch" href="//fonts.googleapis.com/">

	<title><?php wp_title(); ?></title>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>

	<div class="page-wrapper">

		<header class="header">

			<div class="container">
				<a class="logo" href="<?php echo site_url() ?>"><img src="<?php echo $naj_functions->imgURL('logo.png'); ?>" alt="<?php bloginfo('name') ?>" /></a>

				<div class="nav-wrap">
					<?php if( is_user_logged_in() ): ?>
						<a href="<?php echo get_permalink(65); ?>" class="link-button purple"><?php _e('Mon compte'); ?></a>
					<?php else: ?>
						<a href="<?php echo get_permalink(57); ?>" class="link-button purple"><?php _e('Connexion / Inscription'); ?></a>
					<?php endif; ?>
					<?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'container' => 'nav', 'container_class' => 'main-nav-wrap', 'fallback_cb' => 'false' )); ?>
				</div>

				<?php get_template_part('parts/mobile-menu'); ?>
			</div>
		</header>

		<div class="main">
