<?php get_header(); ?>

	<?php get_template_part('parts/page-header' ); ?>

		<section class="content">

			<?php if( !ms_has_membership(85,770,771,772,1064) ):  ?>
				<?php _e('not'); ?>
			<?php else: ?>
				<div class="monthly-content member-layouts">
					<div class="account-menu">
						<?php wp_nav_menu( array( 'theme_location' => 'account-nav', 'container' => 'account-nav', 'container_class' => 'account-nav-wrap', 'fallback_cb' => 'false' )); ?>
					</div>
					<div class="member-content">

						<?php $args = array(
							'post_type' => 'contenu-mois',
							'posts_per_page' => '-1'
							);
							$the_query = new WP_Query( $args );
						?>
						<?php if( $the_query->have_posts() ) : ?>
							<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<h3><span><?php the_title(); ?></span></h3>
								<div class="monthly-archive">
									<?php if ( have_rows('monthly_content') ) : $gift = 1; $colnumber = 1; ?>
										<?php while ( have_rows('monthly_content') ) : the_row(); ?>
											<div class="monthly-content <?php if ($colnumber % 2 == 0) { echo 'even'; } else { echo 'odd'; } ?>">
												<?php

												if (get_sub_field('choice') == 'video') {
													$posts = get_sub_field('video_content');
												} else {
													$posts = get_sub_field('product_content');
												}

												if( $posts ): ?>
													<?php foreach( $posts as $post): ?>
												   <?php setup_postdata($post); ?>
														<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
														<h4>Contenu&nbsp;#<?php echo $gift; ?></h4>
														<p class="description">
															<?php
																$thetitle = get_the_title();
																$getlength = strlen($thetitle);
																$thelength = 25;
																echo substr($thetitle, 0, $thelength);
																if ($getlength > $thelength) echo "...";
															?>
														</p>
													 <a class="button" href="<?php the_permalink(); ?>"><?php _e('Ouvrir'); ?></a>
													<?php endforeach; $gift++; $colnumber++; ?>
													<?php wp_reset_postdata(); ?>
												<?php endif; ?>

											</div>
										<?php endwhile; ?>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>

		</section>

<?php get_footer(); ?>
