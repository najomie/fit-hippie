<?php get_header(); ?>

<?php
	global $naj_functions;
	$banner = get_field('post_banner', 'options');
	if( is_singular( 'podcast' ) ){
		$banner = get_field('podcast_banner', 'options');
	}
?>
<section class="page-banner" style="background-image:url(<?php echo $banner['url']; ?>)">
	<?php if( is_singular( 'podcast' ) ): ?>
		<div class="type-section">
			<img src="<?php echo $naj_functions->imgURL('podcast-logo.png'); ?>" alt="" />
		</div>
	<?php endif; ?>
</section>

<section class="content container-fluid">

	<div class="row post-flex">

		<div class="post-single">
			<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

				<article class="post">

					<h1><?php the_title(); ?></h1>

					<aside class="meta">
						<span class="loop-date"><?php the_time('j F Y') ?></span>
						<span class="loop-cats"><?php the_category(', '); ?></span>
					</aside>

					<?php if ( has_post_thumbnail() ) : ?>
					<figure class="post-thumbnail">
						<?php the_post_thumbnail(' '); ?>
					</figure>
					<?php endif; ?>

					<?php the_content(); ?>

				</article>

			<?php endwhile; endif; ?>

			<div class="pagination"><?php $naj_functions->archive_pagination(); ?></div>

		</div>

		<aside class="blog-sidebar">
			<?php dynamic_sidebar( 'blog-sidebar' ); ?>
		</aside>

	</div>

</section>

<?php get_footer();
