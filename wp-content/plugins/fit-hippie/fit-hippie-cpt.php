<?php
/*
Plugin Name: Fit Hippie - Content of the month
Description: Custom post type for content of the month.
Text Domain: theme
Version: 1.0
Author: Najomie
*/

/* //////////////////////////// Taxonomies and Custom Post Types */

/* ------------------- Custom Post Type: Content of the month */

add_action('init', 'create_cpt_content_month', 0);
function create_cpt_content_month() {
    $labels = array(
        'name'                => __('Contenu du mois', 'theme'),
        'singular_name'       => __('Contenu du mois', 'theme'),
        'menu_name'           => __('Contenu du mois', 'theme'),
        'parent_item_colon'   => __('Contenu parent :', 'theme'),
        'all_items'           => __('Tous les contenus', 'theme'),
        'view_item'           => __('Voir le contenu', 'theme'),
        'add_new_item'        => __('Ajouter un contenu', 'theme'),
        'add_new'             => __('Nouveau contenu', 'theme'),
        'edit_item'           => __('Modifier le contenu', 'theme'),
        'update_item'         => __('Mettre à jour le contenu', 'theme'),
        'search_items'        => __('Trouver du contenu', 'theme'),
        'not_found'           => __('Aucun contenu trouvé', 'theme'),
        'not_found_in_trash'  => __('Aucun contenu dans la corbeille', 'theme'),
    );
    $args = array(
        'label'               => _x('Contenu du mois', 'cpt-content-month-label', 'theme'),
        'description'         => _x('Contenu du mois', 'cpt-content-month-description', 'theme'),
        'labels'              => $labels,
        'hierarchical'        => false,
        'can_export'          => true,
        'has_archive'         => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 4,
        'publicly_queryable'  => true,
        'public'              => true,
        'show_in_admin_bar'   => true,
        'supports'            => array('title', 'editor', 'thumbnail'),
        'rewrite'           => array( 'slug' => _x( 'contenu-mois', 'URL slug', 'theme' ))
    );

    register_post_type('contenu-mois', $args);
}

/* ------------------- Reset permalinks */

function my_rewrite_flush() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush' );
